import React, { Component } from 'react';
import Content from './components/content';

import './stylesheets/reset.css';
import './stylesheets/App.css';
import './stylesheets/nav.css';
import './stylesheets/content.css';
import './stylesheets/list.css';
import './stylesheets/form.css';

import logo from './logo.png';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageType: 'list',
    };
    this.togglePageType = this.togglePageType.bind(this);
  }

  togglePageType(e) {
    if (e.currentTarget.textContent === "Find an Event") {
      this.setState({ pageType: 'list' });
    } else {
      this.setState({ pageType: 'form' });
    }
  }

  render() {
    return (
      <section className="App">
        <header>
          <div className="header-container">
            <img src={logo} alt="logo" />
            <nav>
              <ul>
                <li>
                  <button
                    className="nav-link"
                    onClick={this.togglePageType}
                  >
                    Find an Event
                  </button>
                </li>
                <li>
                  <button
                    className="nav-link"
                    onClick={this.togglePageType}
                  >
                    Add an Event
                  </button>
                </li>
              </ul>
            </nav>
          </div>
        </header>

        <Content pageType={this.state.pageType} />

        <footer />
      </section>
    );
  }
}

export default App;
