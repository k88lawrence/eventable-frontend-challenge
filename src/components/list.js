import React, { Component } from 'react';
import Moment from 'moment';
import * as APIUtil from '../util';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      events: [],
      search: '',
      noResults: false,
    };

    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.toggleTitleOrder = this.toggleTitleOrder.bind(this);
    this.toggleDateOrder = this.toggleDateOrder.bind(this);
  }

  componentDidMount() {
    this.fetchEvents();
  }

  getEventListItems() {
    if (this.state.events.length > 0) {
      return this.state.events.map((event) => {
        return (
          <li
            className="list-item"
            key={event.id}
          >
            <ul className="list-item-info">
              <li>
                <a href={event.url}>
                  {event.title}
                </a>
              </li>
              <li>{ Moment(event.start_time).format('dddd, MMMM Do YYYY, h:mm A')}</li>
            </ul>
          </li>
        );
      });
    } else if (this.state.noResults === true) {
      return <li>No Results :c</li>;
    } else {
      return <div className="loader" />;
    }
  }

  handleSearchChange(e) {
    const search = e.target.value.toLowerCase();
    if (search === '') {
      this.fetchEvents();
    } else {
      const matchingEvents = this.state.events.filter((event) => {
        return event.title.toLowerCase().includes(search);
      });
      if (matchingEvents.length > 0) {
        this.setState({ events: matchingEvents });
      } else {
        this.setState({
          events: [],
          noResults: true,
        });
      }
    }
  }

  fetchEvents() {
    APIUtil.fetchEvents((events) => {
      if (localStorage.events) {
        const localEvents = localStorage.events.split('+');
        localEvents.forEach((event) => {
          events.results.push(JSON.parse(event));
        });
      }
      this.setState({ events: events.results });
    });
  }

  toggleTitleOrder(e) {
    const type = e.currentTarget;
    if (type.classList.length === 0) {
      type.classList.add('down');
      const titleSorted = this.state.events.sort((a, b) => {
        const titleA = a.title.toUpperCase();
        const titleB = b.title.toUpperCase();
        if (titleA < titleB) {
          return -1;
        }
        if (titleA > titleB) {
          return 1;
        }
        return 0;
      });
      this.setState({ events: titleSorted });
    } else if (type.classList.contains('up')) {
      type.classList.add('down');
      type.classList.remove('up');
      const titleSorted = this.state.events.sort((a, b) => {
        const titleA = a.title.toUpperCase();
        const titleB = b.title.toUpperCase();
        if (titleA < titleB) {
          return -1;
        }
        if (titleA > titleB) {
          return 1;
        }
        return 0;
      });
      this.setState({ events: titleSorted });
    } else {
      type.classList.add('up');
      type.classList.remove('down');
      const titleSorted = this.state.events.sort((b, a) => {
        const titleA = a.title.toUpperCase();
        const titleB = b.title.toUpperCase();
        if (titleA < titleB) {
          return -1;
        }
        if (titleA > titleB) {
          return 1;
        }
        return 0;
      });
      this.setState({ events: titleSorted });
    }
  }

  toggleDateOrder(e) {
    const type = e.currentTarget;
    if (type.classList.length === 0) {
      type.classList.add('down');
      const dateSorted = this.state.events.sort((a, b) => {
        const dateA = a.start_time;
        const dateB = b.start_time; // ignore upper and lowercase
        if (dateA < dateB) {
          return -1;
        }
        if (dateA > dateB) {
          return 1;
        }
        return 0;
      });
      this.setState({ events: dateSorted });
    } else if (type.classList.contains('up')) {
      type.classList.add('down');
      type.classList.remove('up');
      const dateSorted = this.state.events.sort((a, b) => {
        const dateA = a.start_time;
        const dateB = b.start_time; // ignore upper and lowercase
        if (dateA < dateB) {
          return -1;
        }
        if (dateA > dateB) {
          return 1;
        }
        return 0;
      });
      this.setState({ events: dateSorted });
    } else {
      type.classList.add('up');
      type.classList.remove('down');
      const dateSorted = this.state.events.sort((b, a) => {
        const dateA = a.start_time;
        const dateB = b.start_time; // ignore upper and lowercase
        if (dateA < dateB) {
          return -1;
        }
        if (dateA > dateB) {
          return 1;
        }
        return 0;
      });
      this.setState({ events: dateSorted });
    }
  }

  render() {
    return (
      <section className="content-wrapper">
        <div className="content list">
          <div className="title">
            <div>
              <h1>Find an Event</h1>
              <p>Browse events by title and <br />start time</p>
            </div>
            <input
              className="search-input"
              type="text"
              placeholder="Search by event title"
              onChange={this.handleSearchChange}
            />
          </div>
          <ul className="list-body">
            <li>
              <ul className="list-header">
                <li>
                  <button onClick={this.toggleTitleOrder}>Name</button>
                </li>
                <li>
                  <button onClick={this.toggleDateOrder}>Start Time</button>
                </li>
              </ul>
            </li>
            {this.getEventListItems()}
          </ul>
        </div>
      </section>
    );
  }
}

export default List;
