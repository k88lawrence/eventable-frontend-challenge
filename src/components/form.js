import React, { Component } from 'react';

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      start_time: '',
      end_time: '',
      errors: [],
      formType: 'pending',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleFormType = this.toggleFormType.bind(this);
  }

  update(property) {
    return e => this.setState({ [property]: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    const errors = [];
    if (this.state.title === '') {
      errors.push('Title is required');
    }

    if (this.state.start_time === '') {
      errors.push('Start time is required');
    }

    if (this.state.end_time === '') {
      errors.push('End time is required');
    }

    if (this.state.start_time > this.state.end_time) {
      errors.push('Invalid end time');
    }

    if (errors.length === 0) {
      const event = {
        title: this.state.title,
        description: this.state.description,
        start_time: this.state.start_time,
        end_time: this.state.end_time,
        id: Math.random() * 10,
      }
      if (localStorage.events) {
        localStorage.events += '+' + JSON.stringify(event);
      } else {
        localStorage.events = '';
        localStorage.events += JSON.stringify(event);
      }
      this.setState({ errors: [] });
      this.setState({ formType: 'success' });
    } else {
      this.setState({ errors });
    }
  }

  renderErrors() {
    return this.state.errors.map((error) => {
      return <li key={error} className="error">{error}</li>;
    });
  }

  toggleFormType() {
    this.setState({ formType: 'pending' });
  }

  render() {
    if (this.state.formType === 'pending') {
      return (
        <section className="content-wrapper">
          <div className="content form">
            <div className="title">
              <div>
                <h1>Add an Event</h1>
                <p>Add an event of your own!</p>
                <ul>
                  {this.renderErrors()}
                </ul>
              </div>
            </div>
            <form className="form-body">
              <input
                className="title"
                type="text"
                placeholder="Event title"
                onChange={this.update('title')}
              />
              <input
                className="description"
                type="text"
                placeholder="Description"
                onChange={this.update('description')}
              />
              <label>Start Time</label>
              <input
                className="start_time"
                type="datetime-local"
                onChange={this.update('start_time')}
              />
              <label>End Time</label>
              <input
                className="end_time"
                type="datetime-local"
                onChange={this.update('end_time')}
              />
              <button
                type="submit"
                className="submit"
                onClick={this.handleSubmit}
              >
                Submit
              </button>

            </form>
          </div>
        </section>
      );
    } else {
      return (
        <section className="content-wrapper">
          <div className="content form">
            <h1>Success!</h1>
            <button
              type="submit"
              className="submit"
              onClick={this.toggleFormType}
            >
              Add another event
            </button>
          </div>
        </section>
      )
    }
  }
}

export default Form;
