import React from 'react';
import Form from './form';
import List from './list';


function Content(props) {
  if (props.pageType === 'list') {
    return <List />;
  } else {
    return <Form />;
  }
}

export default Content;
